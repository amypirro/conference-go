from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import json
import requests

# Anti-Corruption Layers
# This file contains the code used to make HTTP requests to Pexels and Open Weather App
# Code should return Python dictionaries to be used by views


def get_photo(city, state):
    url = "https://api.pexels.com/v1/search"
    headers = {"Authorization": PEXELS_API_KEY}
    params = {
        "query": f"{city} {state} city",
        "per_page": 1,
    }
    response = requests.get(url, headers=headers, params=params)
    content = json.loads(response.content)

    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except (KeyError, IndexError):
        return {"picture_url": None}


def get_weather_info(conference):
    # Get city and state
    city_name = conference.location.city
    state_code = conference.location.state.abbreviation
    params = {
        "q": f"{city_name},{state_code},US",
        "limit": 1,
        "appid": OPEN_WEATHER_API_KEY,
    }
    # Get latitude and longitude of conference location / Geocoding API
    geo_response = requests.get("http://api.openweathermap.org/geo/1.0/direct", params=params)
    geocoding = json.loads(geo_response.content)

    if len(geocoding) > 0:
        lat = geocoding[0]["lat"]
        lon = geocoding[0]["lon"]

    if len(geocoding) == 0:
        return None

    # Call current weather data API
    weather_params = {
        "lat": lat,
        "lon": lon,
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial",
    }
    weather_response = requests.get("https://api.openweathermap.org/data/2.5/weather", params=weather_params)
    weather_data = json.loads(weather_response.content)

    # Create dictionary containing
    #   temp, temperature in Fahrenheit (imperial)
    #   description
    weather = {
        "temp": weather_data.get("main").get("temp"),
        "description": weather_data.get("weather")[0].get("description"),
    }

    try:
        return weather
    except (KeyError, IndexError):
        return None
